/// @description Insert description here
// You can write your code in this editor

// if the key is held, it will be red.
if(keyboard_get_state(key) == KEY_STATE_HOLD ||
	gamepad_get_state(key) == KEY_STATE_HOLD){
	image_blend = c_red;
} else if(keyboard_get_state(key) == KEY_STATE_RELEASE ||
	gamepad_get_state(key) == KEY_STATE_RELEASE){
	image_blend = c_lime;
} else if(keyboard_get_state(key) == KEY_STATE_PRESS ||
	gamepad_get_state(key) == KEY_STATE_PRESS){
	image_blend = c_yellow;
}  else {
	image_blend = c_white;	
}