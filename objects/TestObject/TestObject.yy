{
    "id": "4f5d207e-25a9-47a8-9858-239b90d7b3cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TestObject",
    "eventList": [
        {
            "id": "8b00c203-7182-4105-911b-3b0f436e84f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4f5d207e-25a9-47a8-9858-239b90d7b3cd"
        },
        {
            "id": "6b1b7a5b-ef06-4958-b3f8-e663f2f85cd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4f5d207e-25a9-47a8-9858-239b90d7b3cd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8f3a0397-e09c-45e3-b618-3d990cca1a13",
    "visible": true
}