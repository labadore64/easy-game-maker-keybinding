{
    "id": "0189b678-0fd8-4656-b10f-aec920b9b877",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "AXManager",
    "eventList": [
        {
            "id": "f75467a9-be23-4675-a6a3-2b6b61c0cc26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0189b678-0fd8-4656-b10f-aec920b9b877"
        },
        {
            "id": "5b5000fa-780a-4c83-be9f-288b21d53b98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "0189b678-0fd8-4656-b10f-aec920b9b877"
        },
        {
            "id": "ce3d99c8-8642-4b07-b6da-0e6c2da9b61c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "0189b678-0fd8-4656-b10f-aec920b9b877"
        },
        {
            "id": "67330d2c-f52a-4bcb-94b0-9db31fda6836",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 112,
            "eventtype": 9,
            "m_owner": "0189b678-0fd8-4656-b10f-aec920b9b877"
        },
        {
            "id": "85e39544-5bdf-4a51-b16a-be7de93d2d00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 113,
            "eventtype": 5,
            "m_owner": "0189b678-0fd8-4656-b10f-aec920b9b877"
        },
        {
            "id": "074f427a-b7e4-4a72-b7c9-5c289fcc5f06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 7,
            "m_owner": "0189b678-0fd8-4656-b10f-aec920b9b877"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}