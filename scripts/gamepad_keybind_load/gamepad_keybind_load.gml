/// @function gamepad_keybind_load(file)
/// @description Loads the state of the gamepad keybind from a JSON file.
/// @param {string} file The file to load from.

var file = file_text_open_read(argument[0]);
var stringer = "";

while (!file_text_eof(file))
{
	stringer+= file_text_readln(file);
}
file_text_close(file);

var save_map = json_decode(stringer);

// clears the current map
// when setting the binding, it will just clear out the map
ds_map_clear(global.gamepad_map);

for(var i = 0; i < global.gamepad_size; i++){
	gamepad_bind_set(global.gamepad_array[i],stringToGamepad(save_map[? global.gamepad_array[i]]))
}

ds_map_destroy(save_map);