/// @function gamepad_bind_get(id)
/// @description Gets the gamepad button of a binding from its ID.
/// @param {string} id The reference ID.

var getval = ds_map_find_value(global.gamepad_map,argument[0])

if(!is_undefined(getval)){
	return getval;
}

return -1;