/// @function stringToGamepad(name)
/// @description Turns a string into a gamepad button
/// @param {string} name The name of the button.

		switch(argument[0]){
			
			case "Button1":
			return gp_face1;
		
			
			case "Button2":
			return gp_face2;
	
			
			case "Button3":
			return gp_face3;
	
			
			case "Button4":
			return gp_face4;
	
			
			case "Down":
			return gp_padd;
	
			
			case "Left":
			return gp_padl;
	
			
			case "Right":
			return gp_padr;
	
			
			case "Up":
			return gp_padu;
	
			case "Start":
			return gp_start;
	
			case "Select":
			return gp_select;
	
			case "Left Shoulder 1":
			return gp_shoulderl;
	
			case "Left Shoulder 2":
			return gp_shoulderlb;
	
			case "Right Shoulder 1":
			return gp_shoulderr;
	
			case "Right Shoulder 2":
			return gp_shoulderrb;
	
			case "Left Stick":
			return gp_stickl;
	
			case "Right Stick":
			return gp_stickr;
		}
		
	
