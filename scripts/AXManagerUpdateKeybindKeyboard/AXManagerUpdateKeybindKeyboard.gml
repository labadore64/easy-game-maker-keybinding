/// @function AXManagerUpdateKeybindKeyboard()
/// @description Updates the state of the keyboard Keybinds for the AXManager.

var key;
var state;

// test the state of all keybound keyboard values
for(var i = 0; i < global.keybind_size; i++){
	key = global.keybind_value_array[i];
	
	// updates the states of the key
	if (keyboard_check_pressed(key)){
		state = KEY_STATE_PRESS
	} 
	else if(keyboard_check(key)){
		state = KEY_STATE_HOLD
	} else if (keyboard_check_released(key)){
		state = KEY_STATE_RELEASE
	} else {
		state = KEY_STATE_NONE
	}
		
	global.key_state_array[i] = state;
}