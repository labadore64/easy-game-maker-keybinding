/// @function AXManagerKeybindCleanUp()
/// @description Cleans the Keybind Data for the AX Manager.

ds_map_clean(global.keybind_map);
ds_map_clean(global.gamepad_map);
