/// @function keyboard_bind_init()
/// @description Initializes the game's default keyboard binding state.

// You can change these to match the controls in your game.
// This is just a suggestion.

keyboard_bind_set("left",vk_left);
keyboard_bind_set("right",vk_right);
keyboard_bind_set("up",vk_up);
keyboard_bind_set("down",vk_down);

keyboard_bind_set("confirm",ord("Z"));
keyboard_bind_set("cancel",ord("X"));

keyboard_bind_set("start",vk_enter);
keyboard_bind_set("select", vk_backspace);

keyboard_bind_set("button1",ord("1"));
keyboard_bind_set("button2",ord("2"));
keyboard_bind_set("button3",ord("Q"));
keyboard_bind_set("button4",ord("W"));
keyboard_bind_set("button5",ord("A"));
keyboard_bind_set("button6",ord("S"));