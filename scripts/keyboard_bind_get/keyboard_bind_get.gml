/// @function keyboard_bind_get(id)
/// @description Gets the keyboard key of a binding from its ID.
/// @param {string} id The reference ID.

var getval = ds_map_find_value(global.keybind_map,argument[0])

if(!is_undefined(getval)){
	return getval;
}

return -1;