/// @function ds_map_clean(map)
/// @description Checks if the ds_map exists, then destroys it.
/// @param {real} map The ds_map to destroy.

if(ds_exists(argument[0],ds_type_map)){
	ds_map_destroy(argument[0])
}