/// @function gamepad_get_state(index)
/// @description Gets the current state of a gamepad button based on its keybind index. Values are KEY_STATE_NONE, KEY_STATE_PRESS, KEY_STATE_HOLD and KEY_STATE_RELEASE.
/// @param {real} index The index of the keybind.
if(global.gamepad > -1){
	return global.gamepad_state_array[argument[0]];
} else{
	return KEY_STATE_NONE;
}