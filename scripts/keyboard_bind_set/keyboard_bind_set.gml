/// @function keyboard_bind_set(id, virtual_key)
/// @description Adds an id-virtual key pair to the keyboard keybinding definition.
/// @param {string} id The reference ID.
/// @param {real} virtual_key The virtual keyboard key.

if(!is_undefined(global.keybind_map[? argument[0]])){
	ds_map_replace(global.keybind_map,argument[0],argument[1]);
} else {
	global.keybind_array[global.keybind_size] = argument[0];
	global.keybind_value_array[global.keybind_size] = argument[1];
	ds_map_add(global.keybind_map,argument[0],argument[1]);
	global.keybind_size = ds_map_size(global.keybind_map);
}