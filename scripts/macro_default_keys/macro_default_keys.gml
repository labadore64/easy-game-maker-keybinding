// these macros relate to the default keybind order. You can change this to suit your needs.
// This way you can reference the state array directly without using slow ds_maps.

#macro KEYBOARD_KEY_LEFT 0
#macro KEYBOARD_KEY_RIGHT 1
#macro KEYBOARD_KEY_UP 2
#macro KEYBOARD_KEY_DOWN 3

#macro KEYBOARD_KEY_CONFIRM 4
#macro KEYBOARD_KEY_CANCEL 5

#macro KEYBOARD_KEY_START 6
#macro KEYBOARD_KEY_SELECT 7

#macro KEYBOARD_KEY_BUTTON1 8
#macro KEYBOARD_KEY_BUTTON2 9
#macro KEYBOARD_KEY_BUTTON3 10
#macro KEYBOARD_KEY_BUTTON4 11
#macro KEYBOARD_KEY_BUTTON5 12
#macro KEYBOARD_KEY_BUTTON6 13
