/// @function keyboard_get_state(index)
/// @description Gets the current state of a key based on its keybind index. Values are KEY_STATE_NONE, KEY_STATE_PRESS, KEY_STATE_HOLD and KEY_STATE_RELEASE.
/// @param {real} index The index of the keybind.
return global.key_state_array[argument[0]];
