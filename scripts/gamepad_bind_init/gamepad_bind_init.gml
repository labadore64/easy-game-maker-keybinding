/// @function gamepad_bind_init()
/// @description Initializes the game's default gamepad binding state.

// You can change these to match the controls in your game.
// This is just a suggestion.

gamepad_bind_set("left",gp_padl);
gamepad_bind_set("right",gp_padr);
gamepad_bind_set("up",gp_padu);
gamepad_bind_set("down",gp_padd);

gamepad_bind_set("confirm",gp_face1);
gamepad_bind_set("cancel",gp_face2);

gamepad_bind_set("start",gp_start);
gamepad_bind_set("select", gp_select);

gamepad_bind_set("button1",gp_face3);
gamepad_bind_set("button2",gp_face4);
gamepad_bind_set("button3",gp_shoulderl);
gamepad_bind_set("button4",gp_shoulderlb);
gamepad_bind_set("button5",gp_shoulderr);
gamepad_bind_set("button6",gp_shoulderrb);