/// @function keyboardToString(virtual_key)
/// @description Turns a keyboard virtual key into a string.
/// @param {real} virtual_key The virtual key.

	var key = argument[0];
	if(key != 0){
		var test_key = stringUppercaseOnly(chr(key));

		if(string_lettersdigits(test_key) != ""){
			return test_key;
		}
	
		if(key >= 112 && key <=123){
			var keyval = key - 111;
		
			return 	"F" + string(keyval);
		}
	
		if(key >= 96 && key <= 105){
			var keyval = key - 96;
			return "Num " + string(keyval);
		}

		switch(key){
			case vk_control:
			return "CTRL"
		
			case 9:
			return "Tab";
	
			case 20:
			return "Caps Lock";
	
			case 16:
			return "Shift";
	
			case 162:
			return "L-Ctrl";
	
			case 163:
			return "R-Ctrl";
	
			case 164:
			return "L-Alt";
	
			case 165:
			return "R-Alt";
	
			case 32:
			return "Space";
	
			case 37:
			return "Left"
	
			case 38:
			return "Up"
	
			case 39:
			return "Right"
	
			case 40:
			return "Down"
	
			case 13:
			return "Enter"
	
			case 189:
			return "-"
	
			case 187:
			return "+"
	
			case 8:
			return "Backspace"
	
			case 188:
			return "<"
	
			case 190:
			return ">"
	
			case 191:
			return "?"
	
			case 186:
			return ":"
	
			case 222:
			return "\""
	
			case 219:
			return "{";
	
			case 221:
			return "}"
	
			case 220:
			return "|"
	
			case 192:
			return "~"
	
			case 27:
			return "ESC"
		
			case 111:
			return "Num" + " /"
		
			case 106:
			return "Num" + " *"
		
			case 109:
			return "Num" + " -"
		
			case 107:
			return "Num" + " +"
		
			case 110:
			return "Num" + " ."
		}
	}
	return string(key);
