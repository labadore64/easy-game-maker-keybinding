{
    "id": "8f3a0397-e09c-45e3-b618-3d990cca1a13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "TestSprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 1,
    "bbox_right": 56,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9059aec9-d4f4-4393-9454-f0d0fb082a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f3a0397-e09c-45e3-b618-3d990cca1a13",
            "compositeImage": {
                "id": "85f98091-4f46-4db2-bbcf-9ad98898af7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9059aec9-d4f4-4393-9454-f0d0fb082a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48a26bc1-d7f4-413e-a7f5-5a62e8ce440f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9059aec9-d4f4-4393-9454-f0d0fb082a86",
                    "LayerId": "c914fb4a-a425-40e1-8979-2928caf76766"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c914fb4a-a425-40e1-8979-2928caf76766",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f3a0397-e09c-45e3-b618-3d990cca1a13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 10
}